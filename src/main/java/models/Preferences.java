package models;

import io.codearte.jfairy.Fairy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Preferences implements Serializable {
    // Light 0, Dark 1
    private int Theme;
    private List<String> Topics;

    public Preferences() {
        Topics = new ArrayList<String>();
    }

    public Preferences(Fairy fairy) {
        Theme = fairy.baseProducer().randomBetween(0, 1);
        Topics = new ArrayList<String>();
        int numberOfTopics = fairy.baseProducer().randomBetween(1, 5);
        for (int i = 0; i < numberOfTopics; ++i) {
            Topics.add(fairy.textProducer().word());
        }
    }

    public int getTheme() {
        return Theme;
    }

    public void setTheme(int theme) {
        Theme = theme;
    }

    public List<String> getTopics() {
        return Topics;
    }

    public void setTopics(List<String> topics) {
        Topics = topics;
    }
}
