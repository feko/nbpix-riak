package models;

import io.codearte.jfairy.Fairy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserData implements Serializable {
    private String Username;
    private Preferences Preferences;
    private List<ProductRecommendation> RecommendedProducts;
    private Boolean HasAcceptedUseOfCookies;

    public UserData() {
    }

    public UserData(Fairy fairy) {
        Username = fairy.person().getUsername();
        Preferences = new Preferences(fairy);
        RecommendedProducts = new ArrayList<ProductRecommendation>();
        int numberOfRecommendedProducts = fairy.baseProducer().randomBetween(1, 5);
        for (int i = 0; i < numberOfRecommendedProducts; ++i) {
            RecommendedProducts.add(new ProductRecommendation(fairy));
        }
        HasAcceptedUseOfCookies = fairy.baseProducer().trueOrFalse();
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public Preferences getPreferences() {
        return Preferences;
    }

    public void setPreferences(Preferences preferences) {
        Preferences = preferences;
    }

    public List<ProductRecommendation> getRecommendedProducts() {
        return RecommendedProducts;
    }

    public void setRecommendedProducts(List<ProductRecommendation> recommendedProducts) {
        RecommendedProducts = recommendedProducts;
    }

    public Boolean getHasAcceptedUseOfCookies() {
        return HasAcceptedUseOfCookies;
    }

    public void setHasAcceptedUseOfCookies(Boolean hasAcceptedUseOfCookies) {
        HasAcceptedUseOfCookies = hasAcceptedUseOfCookies;
    }
}
