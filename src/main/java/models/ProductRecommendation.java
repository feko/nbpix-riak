package models;

import io.codearte.jfairy.Fairy;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProductRecommendation implements Serializable {
    private String ProductName;
    private String ImageUrl;
    private BigDecimal ProductPrice;
    private Integer ProductsInStock;

    public ProductRecommendation() {
    }

    public ProductRecommendation(Fairy fairy) {
        ProductName = fairy.textProducer().word();
        ImageUrl = fairy.textProducer().randomString(10) + ".jpg";
        ProductPrice = new BigDecimal(fairy.baseProducer().randomBetween(0, 10000));
        ProductsInStock = fairy.baseProducer().randomBetween(0, 100000);
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public BigDecimal getProductPrice() {
        return ProductPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        ProductPrice = productPrice;
    }

    public Integer getProductsInStock() {
        return ProductsInStock;
    }

    public void setProductsInStock(Integer productsInStock) {
        ProductsInStock = productsInStock;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }
}
