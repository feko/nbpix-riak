import com.basho.riak.client.api.RiakClient;
import com.basho.riak.client.api.commands.kv.DeleteValue;
import com.basho.riak.client.api.commands.kv.FetchValue;
import com.basho.riak.client.api.commands.kv.StoreValue;
import com.basho.riak.client.core.RiakCluster;
import com.basho.riak.client.core.RiakNode;
import com.basho.riak.client.core.query.Location;
import com.basho.riak.client.core.query.Namespace;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import io.codearte.jfairy.Fairy;
import models.UserData;

import java.net.UnknownHostException;
import java.util.concurrent.ExecutionException;

public class RiakBenchmarkApplication {
    private static long totalTime = 0;

    public static void main(String[] args) throws UnknownHostException, ExecutionException, InterruptedException {
        RiakCluster riakCluster = setUpCluster();
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

        final Fairy fairy = Fairy.create();
        final RiakClient riakClient = new RiakClient(riakCluster);
        final Namespace userDataNamespace = new Namespace("userData");

        for (int i = 0; i < 10000; ++i) {

            UserData userData = new UserData(fairy);
            Location userDataLocation = new Location(userDataNamespace, String.valueOf(i));
            StoreValue storeValue = new StoreValue.Builder(userData)
                    .withLocation(userDataLocation)
                    .build();
            long start = System.currentTimeMillis();
            StoreValue.Response response = riakClient.execute(storeValue);
            long end = System.currentTimeMillis();
            totalTime += end - start;
        }

        System.out.printf("Total time for inserting 1mil entries: %d ms\n", totalTime);

        FetchValue fetchValue = new FetchValue.Builder(new Location(userDataNamespace, "9999")).build();
        long start = System.currentTimeMillis();
        FetchValue.Response fresponse = riakClient.execute(fetchValue);
        long end = System.currentTimeMillis();
        UserData uData = fresponse.getValue(UserData.class);
        System.out.printf("Total time for reading 1 entry: %d ms\n", end - start);

        DeleteValue deleteValue = new DeleteValue.Builder(new Location(userDataNamespace, "9999")).build();
        start = System.currentTimeMillis();
        FetchValue.Response dresponse = riakClient.execute(fetchValue);
        end = System.currentTimeMillis();
        System.out.printf("Total time for deleting 1 entry: %d ms", end - start);
    }

    private static RiakCluster setUpCluster() throws UnknownHostException {
        // This example will use only one node listening on localhost:10017
        RiakNode node = new RiakNode.Builder()
                .withRemoteAddress("192.168.181.27")
                .withRemotePort(8087)
                .build();

        // This cluster object takes our one node as an argument
        RiakCluster cluster = new RiakCluster.Builder(node)
                .build();

        // The cluster must be started to work, otherwise you will see errors
        cluster.start();

        return cluster;
    }
}
