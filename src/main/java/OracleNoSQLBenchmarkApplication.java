import io.codearte.jfairy.Fairy;
import models.UserData;
import oracle.kv.*;
import org.apache.commons.lang3.SerializationUtils;

import java.net.UnknownHostException;
import java.util.concurrent.ExecutionException;

public class OracleNoSQLBenchmarkApplication {
    private static long totalTime = 0;
    private static long entries = 10;

    public static void main(String[] args) throws UnknownHostException, ExecutionException, InterruptedException {

        // Setup
        final Fairy fairy = Fairy.create();
        String[] hosts = {"192.168.181.28:5000"};
        KVStoreConfig config = new KVStoreConfig("userData", hosts);
        KVStore kvstore = KVStoreFactory.getStore(config);

        for (int i = 0; i < entries; ++i) {

            UserData userData = new UserData(fairy);
            byte[] data = SerializationUtils.serialize(userData);
            long start = System.currentTimeMillis();
            kvstore.put(Key.fromString(String.format("/%d", i)), Value.createValue(data));
            long end = System.currentTimeMillis();
            totalTime += end - start;
        }
        System.out.printf("Total time for inserting 1mil entries: %d ms\n", totalTime);

        long start = System.currentTimeMillis();
        byte[] data = kvstore.get(Key.fromString("/0")).getValue().getValue();
        UserData userData = SerializationUtils.deserialize(data);
        long end = System.currentTimeMillis();
        System.out.printf("Total time for reading 1 entry: %d ms\n", end - start);

        start = System.currentTimeMillis();
        kvstore.delete(Key.fromString("/0"));
        end = System.currentTimeMillis();
        System.out.printf("Total time for deleting 1 entry: %d ms", end - start);
    }
}
